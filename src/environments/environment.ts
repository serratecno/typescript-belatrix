// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyD-3oB2i26HFa3BQu1lLygYyuHOE9qJm-k',
    authDomain: 'typescript-serra.firebaseapp.com',
    databaseURL: 'https://typescript-serra.firebaseio.com',
    projectId: 'typescript-serra',
    storageBucket: 'typescript-serra.appspot.com',
    messagingSenderId: '61265119943',
    appId: '1:61265119943:web:2ca3d1923f509cba'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
